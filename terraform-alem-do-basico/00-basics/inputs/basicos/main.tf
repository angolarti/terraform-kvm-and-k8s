// variable <name>
variable "pais" {
    type        = string
    default     = "Angola"
    description = "Em qual país você está?"
}

variable "idade" {
    type        = number
    description = "Qual é a sua idade?"
}

variable "vat_benguelense" {
    type        = bool
    default     = true
    description = "Vai Benguelense?"
}

// output <name>
output "pais" {
    value       = var.pais
    description = "Seu país"
}

output "idade" {
    value       = var.idade
    description = "Sua idade"
}

output "vat_benguelense" {
    value       = var.vat_benguelense
    description = "Se é Benguelense ou não"
}
