variable "users" {
    type = map(object({
                email = string
                departament = string
            })
    )
    default = {
        "Walter": {
            "email": "angolarti@gmail.com",
            "departament": "Fábrica de Software"
        }
        "Pluralsis": {
            "email": "pluralsislda@gmail.com",
            "departament": "DevOps"
        }
    } 
}

output "all" {
    value = var.users
}

output "walter" {
    value = var.users["Walter"]
}

output "email_de_pluralsis" {
    value = var.users["Pluralsis"].email
}
