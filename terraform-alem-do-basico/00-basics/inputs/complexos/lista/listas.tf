variable "provincias" {
    type    = list(string)
    default = [
        "Bengo",
        "Benguela",
        "Bié",
        "Cabinda",
        "Kuando-Kubango",
        "Kwanza-Norte",
        "Kwanza-Sul",
        "Cunene",
        "Huambo",
        "Huíla",
        "Luanda",
        "Lunda-Norte",
        "Lunda-Sul",
        "Malanje",
        "Moxico",
        "Namibe",
        "Uíge",
        "Zaire"
    ]
}

output "primeira_provincia" {
    value = var.provincias[0]
    description = "description"
}

output "ultima_provincia" {
    value = var.provincias[length(var.provincias) - 1]
    description = "description"
}
