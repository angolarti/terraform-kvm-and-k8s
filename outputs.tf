output "IPS_OUTPUT_CONTROLPLANE" {
  value = libvirt_domain.machines[0].*.network_interface.0.addresses
}

output "IPS_OUTPUT_WORKER_01" {
  value = libvirt_domain.machines[1].*.network_interface.0.addresses
}

output "IPS_OUTPUT_WORKER_02" {
  value = libvirt_domain.machines[2].*.network_interface.0.addresses
}
