import 'dotenv/config'
import express from 'express'
import UserController from './app/controllers/UserController.js'


const app = express()
app.use(express.json())

const PORT = process.env.PORT

app.post('/users', UserController.store)


app.listen(PORT, () => console.log(`Server running at http://localhost:${PORT}`))