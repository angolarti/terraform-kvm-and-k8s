import passwordGenerator from 'password-generator'

import Mail from '../lib/Mail.js'

export default {
    async store(req, res) {
        const { name, email } = req.body

        const user = {
            name,
            email,
            password: passwordGenerator(15, false)
        }

        await Mail.sendMail({
            from: 'Pluralsis <pluralsislda@gmail.com>',
            to: `${name} <${email}>`,
            subject: 'Cadastro de utilizador',
            html: `Olá, ${name}, bem-vindo a Pluralsis lda.`
        })

        return res.status(200).json(user)
    }
}