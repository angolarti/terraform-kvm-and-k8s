import http from 'node:http'

import GPS from 'gps'


const server = http.createServer(handler)

function handler(req, res) {
    console.log('GPS')
    const gps = new GPS;

    gps.on('data', data => {
        console.log(data, gps.state);
    })
}

server.listen(8000, '0.0.0.0')
    .once('listening', () => console.log('http://localhost:8000'))