#!/usr/bin/env bash

set -e

# Steps to Install KVM on Manjaro Linux

echo "Step 1. Check the Hardware Compatibility"
virtualization=$(LC_ALL=C lscpu | grep Virtualization)

echo "Step 2. Checking Kernel Support"
zgrep CONFIG_KVM /proc/config.gz

echo "Step 3. Install KVM on Manjaro"
sudo pacman -S --needed archlinux-keyring
sudo pacman -S --needed virt-manager libvirt qemu vde2 ebtables dnsmasq bridge-utils openbsd-netcat libguestfs
sudo pacman -S --needed ebtables iptables
sudo pacman -S --needed --asdeps swtpm
sudo usermod -aG kvm,libvirt $USER

echo "Step 3.1: Install libguestfs on Arch Linux / Manjaro"
# libguestfs is a set of tools used to access and modify virtual machine (VM) disk images. You can use this for:

# - viewing and editing files inside guests
# - scripting changes to VMs
# - monitoring disk used/free statistics
# - creating guests
# - P2V
# - V2V
# - performing backup e.t.c

echo "Step 4. Enable the libvirtd Service"
sudo systemctl enable libvirtd.service

echo "Step 5. Start the libvirtd Service"
sudo systemctl start libvirtd.service

echo "Step 6. Configure KVM"

sudo sh -c "sed -i s/#unix_sock_group = \"libvirt\"/unix_sock_group = \"libvirt\"/ /etc/libvirt/libvirtd.conf"
sudo sh -c "sed -i s/#unix_sock_ro_perms = \"0777\"/sed -i s/#unix_sock_ro_perms = \"0777\"/ /etc/libvirt/libvirtd.conf"

sudo usermod -a -G libvirt $(whoami)
newgrp libvirt
sudo systemctl restart libvirtd.service

"echo Step 5: Enable Nested Virtualization (Optional)"

### Intel Processor ###
sudo modprobe -r kvm_intel
sudo modprobe kvm_intel nested=1

### AMD Processor ###
sudo modprobe -r kvm_amd
sudo modprobe kvm_amd nested=1

echo "To make this configuration persistent,run:"
echo "options kvm-intel nested=1" | sudo tee /etc/modprobe.d/kvm-intel.conf

echo "Confirm that Nested Virtualization is set to Yes:"

### Intel Processor ###
systool -m kvm_intel -v | grep nested
cat /sys/module/kvm_intel/parameters/nested 

### AMD Processor ###
systool -m kvm_amd -v | grep nested
cat /sys/module/kvm_amd/parameters/nested 

echo "Step 6: Using KVM on Arch Linux / Manjaro"

The KVM service (libvird) should be running and enabled to start at boot.

sudo systemctl start libvirtd
sudo systemctl enable libvirtd
Enable vhost-net kernel module on Ubuntu/Debian.

sudo modprobe vhost_net
echo vhost_net | sudo tee -a /etc/modules
