provider "libvirt" {
  uri = "qemu:///system"
}

# create KVM bridge network 
resource "libvirt_network" "kvm_network" {
  name = "kvmnet"
  mode = "bridge"
  bridge = "virbr0"
  autostart = "true"
}

resource "libvirt_pool" "pluralsislab" {
  name = "pluralsislab"
  type = "dir"
  path = var.libvirt_disk_path
}

resource "libvirt_volume" "ubuntu_qcow2" {
  name = "ubuntu.qcow2"
  pool = libvirt_pool.pluralsislab.name
  source = var.ubuntu_2204_img_file
  format = "qcow2"
}

resource "libvirt_volume" "cluster_nodes" {
  count          = var.hosts
  name           = "node${count.index}.qcow2"
  base_volume_id = libvirt_volume.ubuntu_qcow2.id
  size           = 21474836480 # 20gb
}

resource "libvirt_cloudinit_disk" "commoninit" {
  count = var.hosts
  name  = "commoninit-${var.distros[0]}${count.index}.iso"
  pool  = libvirt_pool.pluralsislab.name

  user_data = templatefile("${path.module}/templates/cloud_init.yml", {
      instance_name       = count.index == 0 ? "control" : "worker-0${count.index}"
      public_key_location = file(var.public_key_location)
  })
  
  network_config = templatefile("${path.module}/templates/network_config.yml", {
     interface = var.interface
     ip_addr   = var.ips[count.index]
     mac_addr  = var.macs[count.index]
  })
}

# Create the machine
resource "libvirt_domain" "machines" {
  count  = var.hosts
  name   = count.index == 0 ? "control" : "worker0${count.index}"
  memory = var.memory
  vcpu   = var.vpcus
  # cpu = {
  #   mode = "host-passthrough"
  # }
  autostart = true
  cloudinit = element(libvirt_cloudinit_disk.commoninit.*.id, count.index)

  network_interface {
    network_name = "kvmnet"
    addresses    = [var.ips[count.index]]
    mac          = var.macs[count.index]
  }
  qemu_agent = true

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = element(libvirt_volume.cluster_nodes.*.id, count.index)
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}

resource "time_sleep" "ansible" {
  create_duration = var.sleep_time
  depends_on = [libvirt_domain.machines]

  provisioner "remote-exec" {
    inline = [
      "sudo rm -f /var/lib/dpkg/lock-frontend"
    ]

    connection {
      type        = "ssh"
      user        = var.ssh_username
      host        = var.ips[0] # "192.168.122.11"
      private_key = file(var.private_key_location)
      timeout     = "2m"
    }
  }

  provisioner "local-exec" {
    command = <<EOF
      sudo sh -c 'echo "control.k8s.local ${var.ips[0]}" >> /etc/hosts'
      sudo sh -c 'echo "worker01.k8s.local ${var.ips[1]}" >> /etc/hosts'
      sudo sh -c 'echo "worker02.k8s.local ${var.ips[2]}" >> /etc/hosts'
      cd ./ansible
      sudo ansible-playbook -i hosts playbook.yaml
    EOF
  }
}
