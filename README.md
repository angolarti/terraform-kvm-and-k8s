# DevOps

## IaaC - Infrastruture as a Code

É nada mais nada menos você administrar toda a sua infraestrutura com código, em vez de entrar nos consoles das cloud e configurar manual.

### Benefícios

- Auditar melhor a Infraestrutura
- Reconstruir ela toda em cado de problema
- Criação de politicas de Pipeline para conseguir criar recurso de um jeito mais simples e rápido

Terraform vs Ansible

Podemos usar qualquer linguagem de programação para fazer essa administração por você

- Ansible: Não tem noção do que ele já aplicou independente de ter indepotência
- Terraform: tem estados de tudo que ele aplicou ele consegue verificar o que já foi feito

### Terraform.tfstate

É o responsável por manter o estado da infraestrutura

```json
{
    "version": 4,
    "terraform_version": "0.15.1",
    "serial": 1,
    "lineage": "345031e2-c220-cd97-a4e6-afb8fad6c52f",
    "outputs": {
        "pais": {
            "value": "Angola",
            "type": "string"
        }
    },
    "resources": []
}
```

Declarativo vs Procedural

Declarativo: declaras uma intenção e a ferramenta se vira
Procedural: Deves saber as etapas antes de sair fazendo as coisas

#### Exporando o HCL

- **Provaider block**

Ele o bloco responsável pela conexão para algum provedor de cloud ou não

- GCP
- AWS
- Azure
- Kuberntes
- Oracle Cloud
- Alibaba Cloud

```tf
provider "algum_provaider" {
    // Inputs para o provider
}
```

Eg: AWS

```tf
provider "aws" {
    region = "us-east-2",
    profile = "awsouza"
}
```

- **Resource block**

Ele é responsável por criar recursos no provedor alvo, provider_tipo ele significa que todo recurso é prefixado com o nome do provider e o nome é uma referência interna para o terraform.

```tf
resource "provider_tipo" "nome" {
    // Inputs para o resource
}
```

Eg: AWS

```tf
resource "aws_instance" "web" {
    ami           = data.aws_ami.ubuntu.is
    instance_type = "t2.micro"
}
```

- Acessar um valor de um resource
  - tipo.nome.algum_output
    - aws_instance.web.arn


- **Variable e Output block**

Eles se a semelham ou bloco de resource os nomes são apenas referências para o terraform usa-se o type para força o tipo da variável, se não é passado o valor padrão ele deve ser passada em tempo de execução do terraform.

```tf
variable "nome_de_uma_variável" {
    tipo = "tipo da variável. string, number, boll, etc.",
    default = "Essa variável tem algum valor padrão, ou sempre deve ser passada?"
}
```

```tf
variable "nome_do_seu_output" {
    value = "Algum output de um resource, módulo ou variável"
}
```

Exemplo

```tf
variable "idade" {
    tipo        = number,
    default     = 28,
    description = "Qual é a sua idade?
}
```

```tf
variable "idade" {
    value       = var.idade,
    description = "Sua idade"
}
```

- **Data block**

É responsável para fazer buscas em tempo de execução no provider

```tf
data "provider_tipo" "nome" {
   // Inputs para o data
}
```

Exemplo

```tf
data "aws_ami" "ubuntu" {
    most_recent = true

    filter {
        name    = "name"
        values  = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }

    filter {
        name  = "virtualization-type"
        value = ["hvm"]
    }

    owners = ["09972010977"] # Canonical
}
```

- **Module block**

è muito utilizado para reutilização de código ele agrupa vários arquivos terraform

```tf
module "name" {
    source = "/caminho/para/o/seu/modulo" # pode ser interno ou externo (github)
    // Inputs - variáveis para flexibilidade na reutilização e ele podem ser acesso por fora
}
```

Exemplo

```tf
module "vpc" {
    source = "terraform-aws-modules/vpc/aws

    name = "my-vpc"
    cidr = "10.0.0.0/16"

    azs             = ["eu-west-1a", ""eu-west-1b", "eu-west-1c"]
    private_subnets = ["10.0.1.0/16", "10.0.2.0/16", "10.0.3.0/16"]
    public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

    enable_nat_gateway = true
    enable_vpn_gateway = true

    tags = {
        Terraform  = "true"
        Enviroment = "dev"
    }
}
```

### Destrichando o Variables | Terraform