variable hosts {
  type    = number
  default = 3
}

variable "distros" {
  type = list
  default = ["ubuntu", "centos", "opensuse"]
}

variable private_key_location {
  type    = string
  default = "/home/walter/.ssh/id_rsa_terraformlab"
}

variable public_key_location {
  type    = string
  default = "/home/walter/.ssh/id_rsa_terraformlab.pub"
}

variable "libvirt_disk_path" {
  type        = string
  description = "path for libvirt pool"
  default     = "/opt/kvm/pool1"
}

variable "ubuntu_2204_img_file" {
  type        = string
  description = "ubuntu 22.04 image"
  default     = "file:///var/lib/libvirt/images/focal-server-cloudimg-amd64.img"
}

variable "ssh_username" {
  type        = string
  description = "the ssh user to use"
  default     = "manager"
}

variable "controlplane_hostname" {
  type        = string
  description = "the control plane host master"
  default     = "control"
}

variable "worker_hostname" {
  type        = string
  description = "worker node host"
  default     = "worker"
}

variable "interface" {
  type = string
  default = "ens01"
}

variable "ips" {
  type = list
  default = ["192.168.122.11", "192.168.122.22", "192.168.122.33"]
}

variable "macs" {
  type = list
  default = ["52:54:00:50:99:c5", "52:54:00:0e:87:be", "52:54:00:9d:90:38"]
}

variable "become_sudo_password" {
  type = string
  default = "YW5nb2xhcjIwMEAK"
}

variable "sleep_time" {
  type = string
  default = "75s"
}

variable memory {
  type = number
  default = 6144 # 6Gb
}

variable vpcus {
  type = number
  default = 2 # 6Gb
}